package spring.boot.no.deps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootNoDepsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootNoDepsApplication.class, args);
	}

}
