package kafka.receiver;

import com.alibaba.fastjson.JSON;
import kafka.beans.Message;
import kafka.constant.TopicConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaReceiver {

    private static final Logger log = LoggerFactory.getLogger(KafkaReceiver.class);

    @KafkaListener(topics = {TopicConst.EXECUTOR_TOPIC})
    public void listen(String message) {
        Message msg = JSON.parseObject(message, Message.class);
        log.info("接收消息：接收成功，message is: [" + msg + "]");
    }
}
