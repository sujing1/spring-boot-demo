package rest.producer.controller;

import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author guo
 */
@RestController
public class ProducerController {

    @GetMapping("hello")
    public String hello(){
        return "hello producer";
    }

    @GetMapping("hello/param")
    public String helloParams(@RequestParam("name") String name, @RequestParam("value") String value){
        return "hello producer name="+name+" , value="+value;
    }

    @PostMapping("hello/post")
    public String post(){
        return "hello post";
    }


    @PostMapping("hello/post/param")
    public String postParams(@RequestBody Map map){
        String name = map.get("name").toString();
        String value = map.get("value").toString();
        return "hello post name="+name+" , value="+value;
    }
}
