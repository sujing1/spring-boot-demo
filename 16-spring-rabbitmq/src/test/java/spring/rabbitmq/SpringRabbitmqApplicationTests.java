package spring.rabbitmq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import spring.rabbitmq.object.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringRabbitmqApplicationTests {

    @Autowired
	private spring.rabbitmq.onetoone.OneToOneMessageSender oneToOneSender;

	@Test
	public void testOneToOne() throws InterruptedException {
        System.out.println();
        System.out.println("======= rabbitmq onetoone begin ========");
        oneToOneSender.send();
        System.out.println("======= rabbitmq onetoone end ========");
        System.out.println();
        Thread.sleep(1000);
	}

    @Autowired
    private spring.rabbitmq.onetomany.OneToManyMessageSender oneToManyMessageSender;

    @Test
    public void testOneToMany() throws InterruptedException {
        System.out.println();
        System.out.println("======= rabbitmq onetomany begin ========");
        oneToManyMessageSender.send();
        System.out.println("======= rabbitmq onetomany end ========");
        System.out.println();
        Thread.sleep(3000);
    }

    @Autowired
    private spring.rabbitmq.manytomany.ManyToManyMessageSender1 manyToManyMessageSender1;

    @Autowired
    private spring.rabbitmq.manytomany.ManyToManyMessageSender2 manyToManyMessageSender2;

    @Test
    public void testManyToMany() throws InterruptedException {
        System.out.println();
        System.out.println("======= rabbitmq manytomany begin ========");
        manyToManyMessageSender1.send();
        manyToManyMessageSender2.send();
        System.out.println("======= rabbitmq manytomany end ========");
        System.out.println();
        Thread.sleep(5000);
    }

    @Autowired
    private spring.rabbitmq.object.ObjectSender objectSender;

    @Test
    public void testObject() throws InterruptedException {
        System.out.println();
        System.out.println("======= rabbitmq object begin ========");
        objectSender.send(new User("id123","zhangsan",18));
        System.out.println("======= rabbitmq object end ========");
        System.out.println();
        Thread.sleep(1000);
    }


    @Autowired
    private spring.rabbitmq.topic.TopicSender topicSender;

    @Test
    public void testTopicAB() throws InterruptedException {
        System.out.println();
        System.out.println("======= rabbitmq topicAB begin ========");
        topicSender.send1();
        System.out.println("======= rabbitmq topicAB end ========");
        System.out.println();
        Thread.sleep(1000);
    }

    @Test
    public void testTopicB() throws InterruptedException {
        System.out.println();
        System.out.println("======= rabbitmq topicB begin ========");
        topicSender.send2();
        System.out.println("======= rabbitmq topicB end ========");
        System.out.println();
        Thread.sleep(1000);
    }


    @Autowired
    private spring.rabbitmq.fanout.FanoutSender fanoutSender;

    @Test
    public void testFanout() throws InterruptedException {
        System.out.println();
        System.out.println("======= rabbitmq fanout begin ========");
        fanoutSender.send();
        System.out.println("======= rabbitmq fanout end ========");
        System.out.println();
        Thread.sleep(3000);
    }
}
