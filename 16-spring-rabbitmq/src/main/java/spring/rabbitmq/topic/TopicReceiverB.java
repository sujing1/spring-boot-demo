package spring.rabbitmq.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = TopicRabbitConfig.QUEUE_B)
public class TopicReceiverB {
    @RabbitHandler
    public void process(String message) {
        System.out.println("ReceiverB : " + message);
    }
}
