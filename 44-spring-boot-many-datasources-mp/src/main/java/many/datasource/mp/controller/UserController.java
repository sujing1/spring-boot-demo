package many.datasource.mp.controller;

import many.datasource.mp.entity.User;
import many.datasource.mp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/insert/{id}/{name}")
    public int insertUser(@PathVariable String id, @PathVariable String name){
        User user = new User();
        user.setId(id);
        user.setName(name);
        return userService.insertUser(user);
    }

    @GetMapping("/selectUser1")
    public List<User> selectUser1(){
        return userService.selectUser1();
    }

    @GetMapping("/selectUser2")
    public List<User> selectUser2(){
        return userService.selectUser2();
    }

    @GetMapping("/selectUser3")
    public List<User> selectUser3(){
        return userService.selectUser3();
    }
}
