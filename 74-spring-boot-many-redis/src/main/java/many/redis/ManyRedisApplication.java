package many.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManyRedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManyRedisApplication.class, args);
	}

}

