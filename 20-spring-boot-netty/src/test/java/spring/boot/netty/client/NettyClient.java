package spring.boot.netty.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NettyClient {

    private static final EventLoopGroup group = new NioEventLoopGroup();
    public static void main(String[] args) {
        try {
            Bootstrap client = new Bootstrap();
            client.group(group)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .handler(new NettyClientChannelInitializer());
            // 启动客户端
            ChannelFuture channelFuture = client.connect("127.0.0.1", 7000).sync();


            chat(channelFuture);

            // 等待客户端链路关闭
            channelFuture.channel().close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 优雅退出，释放线程池资源
            group.shutdownGracefully();
        }
    }

    private static void chat(ChannelFuture channelFuture) throws IOException, InterruptedException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String userInput;
        System.out.println("请开始输入：");
        while ((userInput = reader.readLine()) != null) {
            if ("exit".equalsIgnoreCase(userInput)){
                break;
            }
            channelFuture.channel().writeAndFlush(userInput);
        }
    }
}
