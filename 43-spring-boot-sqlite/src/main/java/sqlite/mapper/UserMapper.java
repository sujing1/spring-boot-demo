package sqlite.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import sqlite.entity.User;

public interface UserMapper {

    @Insert("insert into user(id,name) values(#{id},#{name})")
    public int insert(User user);

    @Select("select id,name from user where id=#{id}")
    public User selectById(String id);

    @Delete("delete from user where id=#{id}")
    public Integer deleteById(String id);
}
