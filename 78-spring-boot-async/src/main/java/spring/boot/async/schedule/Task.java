package spring.boot.async.schedule;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Task {
    private static final Log logger = LogFactory.getLog(Task.class);

    @Scheduled(cron = "0/2 * * * * ?")
    public void log1(){
        logger.info("test schedule 111");
    }

    @Scheduled(cron = "0/2 * * * * ?")
    public void log2(){
        logger.info("test schedule 222");
    }

    @Scheduled(cron = "0/2 * * * * ?")
    public void log3(){
        logger.info("test schedule 333");
    }

    @Scheduled(cron = "0/2 * * * * ?")
    public void log4(){
        logger.info("test schedule 444");
    }

    @Scheduled(cron = "0/2 * * * * ?")
    public void log5(){
        logger.info("test schedule 555");
    }

    @Scheduled(cron = "0/2 * * * * ?")
    public void log6(){
        logger.info("test schedule 666");
    }

    @Scheduled(cron = "0/2 * * * * ?")
    public void log7(){
        logger.info("test schedule 777");
    }

    @Scheduled(cron = "0/2 * * * * ?")
    public void log8(){
        logger.info("test schedule 888");
    }

    @Scheduled(cron = "0/2 * * * * ?")
    public void log9(){
        logger.info("test schedule 999");
    }
}
