package out.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guo
 */
@SpringBootApplication
public class OutConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(OutConfigApplication.class, args);
	}

}

