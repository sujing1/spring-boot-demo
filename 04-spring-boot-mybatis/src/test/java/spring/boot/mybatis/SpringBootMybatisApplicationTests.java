package spring.boot.mybatis;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootMybatisApplicationTests {

	@Autowired
	private SysUserMapper sysUserMapper;

	@Test
	public void testInsert(){
		SysUser sysUser = new SysUser();
		sysUser.setName("zhangsan");
		int num = sysUserMapper.insert(sysUser);
		System.out.println(num);
	}


	@Test
    public void testFindList(){
	    int pageNum = 2;
	    int pageSize = 2;
        PageHelper.startPage(pageNum, pageSize);
        List<SysUser> sysUserList = sysUserMapper.findList();
        PageInfo<SysUser> sysUserPageInfo = new PageInfo<>(sysUserList);
        for (int i = 0; i < sysUserPageInfo.getList().size(); i++) {
            System.out.println("id=   "+sysUserPageInfo.getList().get(i).getId()+"  name=  "+sysUserPageInfo.getList().get(i).getName());
        }
    }

}
