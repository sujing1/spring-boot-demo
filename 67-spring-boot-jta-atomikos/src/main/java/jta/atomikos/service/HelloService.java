package jta.atomikos.service;

import jta.atomikos.model.one.One;
import jta.atomikos.model.two.Two;
import jta.atomikos.one.mapper.OneMapper;
import jta.atomikos.two.mapper.TwoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author guo
 */
@Service
public class HelloService {

    @Autowired(required = false)
    private OneMapper oneMapper;

    @Autowired(required = false)
    private TwoMapper twoMapper;



    @Transactional(rollbackFor = Exception.class)
    public int insert(One one, Two two){
        int num = 0;
        num = num + twoMapper.insert(two);
        num = num + oneMapper.insert(one);
        return num;
    }

}
