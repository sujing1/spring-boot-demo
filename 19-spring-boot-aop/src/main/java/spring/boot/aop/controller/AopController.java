package spring.boot.aop.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.boot.aop.aop.AopMethod;


/**
 * @author blues
 */
@RestController
public class AopController {

    @AopMethod
    @RequestMapping("/aop/test")
    public Object test() {
        System.out.println("aop controller test method print......");
        return "test";
    }

    @RequestMapping("/aop/errors")
    public Object errors() {
        return 1 / 0;
    }
}
